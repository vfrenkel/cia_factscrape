Of the following:

Requirements Engineering
Formal Design (e.g., UML, CRCs, Design Patterns)
Software Process (e.g., Waterfall, Agile)
Software Testing (e.g., Black Box, White Box, Acceptance Testing)
Software Quality Assurance (e.g., Code Inspection, Refactoring)
Software Component Models (e.g., External Libraries, EJB, Web Services)
Software Tools (e.g., IDEs, Version Control Systems, Bug Repositories)


- I used Requirements Engineering. I created a file called requirements_and_design.txt before starting to code where I planned out what kind of syntax I would support in order to meet the requirements laid out by the assignment.

- I'm not sure what Formal Design is. I suppose I used a Design Pattern when I decided to separate the interface from the library performing the work.

- For my software process I used a hybrid waterfall/agile approach. First I planned out as much of the software as I could, then I modified requirements/design plans as I coded the software in an iterative try/revise way.

- I didn't write a software testing suite for this project. The scope of the project seemed small enough not to require it. I did test each piece of my code as I wrote it using inline print statements.

- I used external libraries and python standard library's urllib to handle the dirty work of accessing DOM elements and fetching data from the various urls.

- I used emacs as my IDE (linux could also be considered an IDE). I used git to keep revisions of my work. I didn't feel like the scope of this project required a bug tracker so I didn't use one.


Some more design details can be found in requirements_and_design.txt in the design folder.
